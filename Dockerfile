FROM erlang:22-alpine

ADD . /opt/furtheworld
WORKDIR /opt/furtheworld
RUN rebar3 release -d false
CMD ["/opt/furtheworld/_build/default/rel/furtheworld/bin/furtheworld-0.3", "console"]