# FurTheWorld

A revolutionary furry fandom platform ... create and connect.

## Build Process

The SQL procedures are currently proprietary. Define a password salt in `Msal.src` and MySQL credentials in `Mcredentials.src`.

```
$ rebar3 compile
```

## Using Docker

Because it is awesome. FurTheWorld uses a private registry.