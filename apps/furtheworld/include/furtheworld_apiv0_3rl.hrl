%%%-------------------------------------------------------------------
%% @doc furtheworld API v0.3 rate limiter formats
%% @end
%%%-------------------------------------------------------------------

%% The state of the rate limiter:
%% -- tries = Number of tries this peer has attempted
%% -- peer = The peer.
-record(furtheworld_apiv0_3rl_t, {
    tries :: non_neg_integer(),
    peer :: {inet:ip_address(), inet:port_number()}
}).