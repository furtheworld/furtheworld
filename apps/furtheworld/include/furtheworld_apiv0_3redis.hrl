%%%-------------------------------------------------------------------
%% @doc furtheworld API v0.3 Redis endpoint formats
%% @end
%%%-------------------------------------------------------------------

%% Cachey method for getting the Redis cache?
%% -- redis = the redis cluster
%% -- redisps = the redis cluster, pubsub
%% -- mnotice = Do we have a notice?
%% -- mtitle = What is our notice title?
%% -- mbody = What is our notice body?

-record(furtheworld_apiv0_3redis_t, {
  redis :: pid(),
  redisps :: pid(),
  mnotice :: boolean(),
  mtitle :: binary(),
  mbody :: binary()
}).