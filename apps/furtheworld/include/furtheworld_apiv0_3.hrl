%%%-------------------------------------------------------------------
%% @doc furtheworld API v0.3 formats
%% @end
%%%-------------------------------------------------------------------

-include("furtheworld_apiv0_3ds.hrl").

%% API listener response
-record(furtheworld_apiv0_3_r, {
  p :: #furtheworld_apiv0_3ds_t{},
  r :: term(),
  e :: binary() | 'undefined',
  tries :: integer()
}).