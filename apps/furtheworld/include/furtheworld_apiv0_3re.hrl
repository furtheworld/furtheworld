%%%-------------------------------------------------------------------
%% @doc furtheworld API v0.3 route endpoint formats
%% @end
%%%-------------------------------------------------------------------

-include("furtheworld_apiv0_3.hrl").

%% The state of the API endpoint:
%% -- htargs = The args used in the HTTP request
%% -- peer = The peer, IP and port, used to connect
-record(furtheworld_apiv0_3re_t, {
  hmethod :: binary(),
  hfield :: binary(),
  hsubfield :: binary(),
  peer :: {inet:ip_address(), inet:port_number()}
}).