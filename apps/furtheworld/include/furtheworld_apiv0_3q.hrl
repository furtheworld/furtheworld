%%%-------------------------------------------------------------------
%% @doc furtheworld API v0.3 queue formats
%% @end
%%%-------------------------------------------------------------------

%% The state of the queues:
%% -- q_rl = Rate limiters
%% -- q_re = Routing endpoints
-record(furtheworld_apiv0_3q_t, {
  q_rl :: map(),
  q_re :: map()
}).