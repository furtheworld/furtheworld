%%%-------------------------------------------------------------------
%% @doc furtheworld API v0.3 datesystem formats
%% @end
%%%-------------------------------------------------------------------

%% A record of the Date Subsystem, a granular span:
%% -- dt = Date and time, coarse, granular to a second
%% -- ms = Milliseconds, can be undefined if so needed

-record(furtheworld_apiv0_3ds_t, {
  dt :: calendar:datetime(),
  ms :: 0..999
}).