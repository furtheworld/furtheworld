%%%-------------------------------------------------------------------
%% @doc furtheworld API v0.3 authentication endpoint formats
%% @end
%%%-------------------------------------------------------------------

-include("furtheworld_apiv0_3.hrl").

%% The state of the API endpoint:
%% -- route = Request state
%% -- cool = Cooldown, back off. Use fuzzy logic.
%% -- tries = How many times we used the endpoint within a span.
-record(furtheworld_apiv0_3ae_t, {
  user :: binary(),
  route :: #furtheworld_apiv0_3re_t{},
}).