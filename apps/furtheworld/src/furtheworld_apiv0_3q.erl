%%%-------------------------------------------------------------------
%% @doc furtheworld API v0.3 queue
%% @end
%%%-------------------------------------------------------------------
-module(furtheworld_apiv0_3q).

-behaviour(gen_statem).

-include("furtheworld_apiv0_3q.hrl").

-export([callback_mode/0, init/1, start/1]).

-export([accepting/3, throttling/3]).

-define(SERVER, ?MODULE).

-define(LIMITS, 128).

start(Args) ->
    gen_statem:start_link({local, ?SERVER}, ?MODULE, Args,
			  []).

init(_Args) ->
    {ok, accepting,
     #furtheworld_apiv0_3q_t{q_rl = #{}, q_re = #{}}}.

accepting({call, From}, {get_or_spawn_re, _, _},
	  #furtheworld_apiv0_3q_t{q_re = QRE})
    when map_size(QRE) > (?LIMITS) - 1 ->
    {keep_state_and_data,
     [{reply, From, {error, throttled}}]};
accepting({call, From}, {get_or_spawn_rl, _},
	  #furtheworld_apiv0_3q_t{q_rl = QRL})
    when map_size(QRL) > (?LIMITS) - 1 ->
    {keep_state_and_data,
     [{reply, From, {error, throttled}}]};
%% Can accept now.
accepting({call, From},
	  {get_or_spawn_rl, {PeerIP, PeerPort}},
	  #furtheworld_apiv0_3q_t{q_rl = QRL} = Data) ->
    P0 = supervisor:start_child(furtheworld_sup,
				#{id =>
				      {furtheworld_apiv0_3rl,
				       {PeerIP, PeerPort}},
				  start =>
				      {furtheworld_apiv0_3rl, start,
				       [{PeerIP, PeerPort}]}}),
    case P0 of
      {ok, P1} ->
	  {keep_state,
	   Data#furtheworld_apiv0_3q_t{q_rl = QRL#{PeerIP => P1}},
	   [{reply, From, {ok, P1}}]};
      {error, {already_started, P1}} ->
	  {keep_state_and_data, [{reply, From, {ok, P1}}]}
    end;
accepting({call, From},
	  {get_or_spawn_re, {PeerIP, PeerPort}, HTArgs},
	  #furtheworld_apiv0_3q_t{q_re = QRE} = Data) ->
    P0 = supervisor:start_child(furtheworld_sup,
				#{id =>
				      {furtheworld_apiv0_3re,
				       {PeerIP, PeerPort}},
				  start =>
				      {furtheworld_apiv0_3re, start,
				       [{{PeerIP, PeerPort}, HTArgs}]},
				  restart => temporary}),
    case P0 of
      {ok, P1} ->
	  {keep_state,
	   Data#furtheworld_apiv0_3q_t{q_re = QRE#{PeerIP => P1}},
	   [{reply, From, {ok, P1}}]};
      {error, {already_started, P1}} ->
	  {keep_state_and_data, [{reply, From, {ok, P1}}]}
    end.

%% Throttler

throttling({call, From}, _, _) ->
    {keep_state_and_data,
     [{reply, From, {error, throttled}}]}.

callback_mode() -> state_functions.
