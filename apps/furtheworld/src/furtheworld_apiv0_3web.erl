%%%-------------------------------------------------------------------
%% @doc furtheworld API v0.3 handler
%% @end
%%%-------------------------------------------------------------------

-module(furtheworld_apiv0_3web).

-export([init/2]).

send_response(Code, JSON, T, Req, T0) ->
    T1 = furtheworld_apiv0_3ds:now(),
    TS = furtheworld_apiv0_3ds:get_span(null, T1, T0),
    cowboy_req:reply(Code,
		     #{<<"content-type">> => <<"application/json">>,
		       <<"access-control-allow-origin">> =>
			   <<"https://www.furtheworld.net">>},
		     mochijson2:encode({[{t,
					  {[{cur, T},
					    {max,
					     furtheworld_apiv0_3rl:max()}]}},
					 {latency,
					  iolist_to_binary(furtheworld_apiv0_3ds:stringify_to_iolist(TS))},
					 JSON]}),
		     Req).

init(Req0, State) ->
    T0 = furtheworld_apiv0_3ds:now(),
    #{peer := {PeerIP, PeerPort}, method := Method} = Req0,
    #{field := Field, subfield := Subfield} =
	cowboy_req:bindings(Req0),
    User = case cowboy_req:parse_qs(Req0) of
	     [{<<"user">>, U}] -> U;
	     _ -> undefined
	   end,
    {ok, RL} = gen_statem:call(furtheworld_apiv0_3q,
			       {get_or_spawn_rl, {PeerIP, PeerPort}}),
    {ok,
     case gen_statem:call(RL, bump) of
       {ok, {throttle, T}} ->
	   {ok, Body, Req1} = cowboy_req:read_body(Req0,
						   #{length => 4096}),
	   {ok, RE} = gen_statem:call(furtheworld_apiv0_3q,
				      {get_or_spawn_re, {PeerIP, PeerPort},
				       #{method => Method, field => Field,
					 subfield => Subfield, body => Body,
					 quser => User}}),
	   case gen_statem:call(RE, get_result) of
	     {ok, {Code, JSON}} ->
		 send_response(Code, JSON, T, Req1, T0);
	     {cookie, {Code, {CN, CV, CO}, JSON}} ->
		 send_response(Code, JSON, T,
			       cowboy_req:set_resp_cookie(CN, CV, Req1, CO),
			       T0);
	     {error, {Code, Error}} ->
		 send_response(Code, Error, T, Req1, T0)
	   end;
       {ok, rate_limit} ->
	   T1 = furtheworld_apiv0_3ds:now(),
	   TS = furtheworld_apiv0_3ds:get_span(null, T1, T0),
	   logger:notice("Rate limiting event with ~p of latency.~n~p~n",
			 [iolist_to_binary(furtheworld_apiv0_3ds:stringify_to_iolist(TS)),
			  Req0]),
	   cowboy_req:reply(429,
			    #{<<"content-type">> => <<"application/json">>,
			      <<"access-control-allow-origin">> =>
				  <<"https://www.furtheworld.net">>,
			      <<"retry_after">> => <<"60">>},
			    mochijson2:encode({[{e, rate_limit}]}), Req0)
     end,
     State}.
