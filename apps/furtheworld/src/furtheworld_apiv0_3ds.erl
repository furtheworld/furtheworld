%%%-------------------------------------------------------------------
%% @doc furtheworld API v0.3 datesystem
%% @end
%%%-------------------------------------------------------------------
-module(furtheworld_apiv0_3ds).

-include("furtheworld_apiv0_3ds.hrl").

-export([now/0]).
-export([stringify_to_iolist/1]).
-export([encode/2]).
-export([get_span/3]).

%% Get the current time.
now() ->
    TS = erlang:timestamp(),
    TSd = calendar:now_to_datetime(TS),
    {_, _, TSu} = TS,
    #furtheworld_apiv0_3ds_t{
        dt = TSd,
        ms = TSu div 1000
    }.
%% If you're lazy then you can use this in `erl` shell.
encode(DT, MS) ->
    stringify_to_iolist(#furtheworld_apiv0_3ds_t{
        dt = DT,
        ms = MS
    }).

stringify_to_iolist(D) ->
    stringify_split(D#furtheworld_apiv0_3ds_t.dt, D#furtheworld_apiv0_3ds_t.ms).

stringify_split({{0, 1, 1}, {0, 0, 0}}, 0) -> [<<"<1ms">>];
stringify_split({{0, 1, 1}, {0, 0, 0}}, Dms) -> [integer_to_binary(Dms), <<"ms">>];
stringify_split(Ddt, 0) -> stringify_split_without_ms(Ddt);
stringify_split(Ddt, Dms) -> [stringify_split_without_ms(Ddt), <<", ">>, integer_to_binary(Dms), <<"ms">>].

stringify_split_without_ms({{0, 1, 1}, {0, 0, DdtTS}}) ->
    [integer_to_binary(DdtTS), <<"sec">>];

stringify_split_without_ms({{0, 1, 1}, {0, DdtTM, DdtTS}}) ->
    [integer_to_binary(DdtTM), <<"min ">>, stringify_split_without_ms({{0, 1, 1}, {0, 0, DdtTS}})];

stringify_split_without_ms({{0, 1, 1}, {DdtTH, DdtTM, DdtTS}}) ->
    [integer_to_binary(DdtTH), <<"hr ">>, stringify_split_without_ms({{0, 1, 1}, {0, DdtTM, DdtTS}})];

%% HACK: Pathetic UNIX bias...
stringify_split_without_ms({{0, 1, DdtDD}, DdtT}) ->
    [integer_to_binary(DdtDD), <<"d, ">>, stringify_split_without_ms({{0, 1, 1}, DdtT})];

%% HACK: Pathetic UNIX bias...
stringify_split_without_ms({{0, DdtDM, DdtDD}, DdtT}) ->
    [integer_to_binary(DdtDM), <<"m ">>, stringify_split_without_ms({{0, 1, DdtDD}, DdtT})];

stringify_split_without_ms({{DdtDY, DdtDM, DdtDD}, DdtT}) ->
    [integer_to_binary(DdtDY), <<"y ">>, stringify_split_without_ms({{0, DdtDM, DdtDD}, DdtT})].

%% Get the span between two points in time and a constant
get_span(unix, D1, D0) when D1#furtheworld_apiv0_3ds_t.ms >= D0#furtheworld_apiv0_3ds_t.ms ->
    SC = calendar:datetime_to_gregorian_seconds({{1970, 1, 1}, {0, 0, 0}}),
    S1 = calendar:datetime_to_gregorian_seconds(D1#furtheworld_apiv0_3ds_t.dt),
    S0 = calendar:datetime_to_gregorian_seconds(D0#furtheworld_apiv0_3ds_t.dt),
    #furtheworld_apiv0_3ds_t {
        dt = calendar:gregorian_seconds_to_datetime(S1 - S0 - SC),
        ms = D1#furtheworld_apiv0_3ds_t.ms - D0#furtheworld_apiv0_3ds_t.ms
    };

get_span(unix, D1, D0) when D1#furtheworld_apiv0_3ds_t.ms < D0#furtheworld_apiv0_3ds_t.ms ->
    SC = calendar:datetime_to_gregorian_seconds({{1970, 1, 1}, {0, 0, 0}}),
    S1 = calendar:datetime_to_gregorian_seconds(D1#furtheworld_apiv0_3ds_t.dt),
    S0 = calendar:datetime_to_gregorian_seconds(D0#furtheworld_apiv0_3ds_t.dt),
    #furtheworld_apiv0_3ds_t {
        dt = calendar:gregorian_seconds_to_datetime(S1 - S0 - SC - 1),
        ms = D1#furtheworld_apiv0_3ds_t.ms - D0#furtheworld_apiv0_3ds_t.ms + 1000
    };

get_span(unix, D1, D0) -> 
    SC = calendar:datetime_to_gregorian_seconds({{1970, 1, 1}, {0, 0, 0}}),
    S1 = calendar:datetime_to_gregorian_seconds(D1#furtheworld_apiv0_3ds_t.dt),
    S0 = calendar:datetime_to_gregorian_seconds(D0#furtheworld_apiv0_3ds_t.dt),
    #furtheworld_apiv0_3ds_t{
        dt = calendar:gregorian_seconds_to_datetime(S1 - S0 - SC),
        ms = 0
    };

%% Get the span between two points in time and a constant
get_span(null, D1, D0) when D1#furtheworld_apiv0_3ds_t.ms >= D0#furtheworld_apiv0_3ds_t.ms ->
    S1 = calendar:datetime_to_gregorian_seconds(D1#furtheworld_apiv0_3ds_t.dt),
    S0 = calendar:datetime_to_gregorian_seconds(D0#furtheworld_apiv0_3ds_t.dt),
    #furtheworld_apiv0_3ds_t {
        dt = calendar:gregorian_seconds_to_datetime(S1 - S0),
        ms = D1#furtheworld_apiv0_3ds_t.ms - D0#furtheworld_apiv0_3ds_t.ms
    };

get_span(null, D1, D0) when D1#furtheworld_apiv0_3ds_t.ms < D0#furtheworld_apiv0_3ds_t.ms ->
    S1 = calendar:datetime_to_gregorian_seconds(D1#furtheworld_apiv0_3ds_t.dt),
    S0 = calendar:datetime_to_gregorian_seconds(D0#furtheworld_apiv0_3ds_t.dt),
    #furtheworld_apiv0_3ds_t {
        dt = calendar:gregorian_seconds_to_datetime(S1 - S0 - 1),
        ms = D1#furtheworld_apiv0_3ds_t.ms - D0#furtheworld_apiv0_3ds_t.ms + 1000
    };

get_span(null, D1, D0) -> 
    S1 = calendar:datetime_to_gregorian_seconds(D1#furtheworld_apiv0_3ds_t.dt),
    S0 = calendar:datetime_to_gregorian_seconds(D0#furtheworld_apiv0_3ds_t.dt),
    #furtheworld_apiv0_3ds_t{
        dt = calendar:gregorian_seconds_to_datetime(S1 - S0),
        ms = 0
    }.