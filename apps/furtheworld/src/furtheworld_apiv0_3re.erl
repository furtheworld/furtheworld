%%%-------------------------------------------------------------------
%% @doc furtheworld API v0.3 routing endpoint
%% @end
%%%-------------------------------------------------------------------
-module(furtheworld_apiv0_3re).

-behaviour(gen_statem).

-include("furtheworld_apiv0_3re.hrl").

-export([callback_mode/0, init/1, start/1]).

-export([launch/3]).

-define(SERVER, ?MODULE).

%% Since this was sloppy before I refactored...
%% This will init a _routing endpoint_ that'll launch some other endpoints.
start({Peer, HTArgs}) ->
    gen_statem:start_link({local, ?SERVER}, ?MODULE,
			  {Peer, HTArgs}, []).

init({Peer, HTArgs}) ->
    #{method := HM, field := HF, subfield := HS} = HTArgs,
    {ok, launch,
     #furtheworld_apiv0_3re_t{peer = Peer, hmethod = string:casefold(HM),
			      hfield = string:casefold(HF), hsubfield = string:casefold(HS)}}.

launch({call, From}, get_result, #furtheworld_apiv0_3re_t{hmethod = <<"get">>, hfield = <<"info">>, hsubfield = <<"motd">>}) ->
    #{n := N, t := T, b := B} = gen_statem:call(furtheworld_apiv0_3redis, get_motd),
    {stop_and_reply, normal, 
    if N
        false -> [{reply, From, {ok, {200, [{notice, false}]}}}}];
        true -> [{reply, From, {ok, {200, {[{notice, true}, {title, T}, {body, B}]}}}}]
    end};

launch({call, From}, get_result, _Data) ->
    {stop_and_reply, normal,
     [{reply, From, {error, {500, {e, unimplemented}}}}]}.

callback_mode() -> state_functions.
