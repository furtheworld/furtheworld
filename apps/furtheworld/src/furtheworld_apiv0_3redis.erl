%%-------------------------------------------------------------------
%% @doc furtheworld API v0.3 Redis endpoint
%% @end
%%%-------------------------------------------------------------------
-module(furtheworld_apiv0_3redis).

-include("furtheworld_apiv0_3redis.hrl").

-export([callback_mode/0, init/1]).

-export([start/1]).

-export([connected/3]).

-behaviour(gen_statem).

-define(SERVER, ?MODULE).

start(Args) ->
    gen_statem:start_link({local, ?SERVER}, ?MODULE, Args,
			  []).

init(_) ->
    {ok, [{redis, Ruri, Rpass}]} =
	file:consult(filename:join([code:priv_dir(furtheworld),
				    "Rcredentials.src"])),
    {ok, Rr} = eredis:start_link(Ruri, 6379, 0, Rpass),
    {ok, Rs} = eredis_sub:start_link(Ruri, 6379, Rpass),
    eredis_sub:psubscribe(Rs, <<"furtheworld/v0_3/*">>),
    {ok, connected,
     #furtheworld_apiv0_3redis_t{redis = Rr, redisps = Rs}}.

connected({call, From}, get_motd,
	  #furtheworld_apiv0_3redis_t{mnotice = Notice,
				      mtitle = Title, mbody = Body}) ->
    {keep_state_and_data,
     [{reply, From,
       #{n => Notice, t => Title, b => Body}}]}.

callback_mode() -> state_functions.
