%%-------------------------------------------------------------------
%% @doc furtheworld API v0.3 SQL endpoint
%% @end
%%%-------------------------------------------------------------------
-module(furtheworld_apiv0_3sql).

-export([get_expiry/1, get_user/1, set_expiry/1]).

% CALL `furtheworld`.`create_user`(rpad("User", 16, 0x0), unhex(sha2("Pass", 512)), rpad("LongUser",64, 0x0), unhex(sha2("Nonce", 512)), @user);
% SELECT @user;

init_mysql() ->
    {ok, [{mysql, Muri, Muser, Mpass}]} =
	file:consult(filename:join([code:priv_dir(furtheworld),
				    "Mcredentials.src"])),
    mysql:start_link([{host, Muri}, {user, Muser},
		      {password, Mpass}]).

get_user({P, H, N, U}) ->
    {ok, M} = init_mysql(),
    {atomic, {ok, [<<"@i">>, <<"@e">>], [[UserID], [Expired]]}} =
	mysql:transaction(M,
			  fun () ->
				  ok = mysql:query(M,
						   <<"CALL `furtheworld`.`check_user`(rpad(?,16,0x0"
						     "),?,?,@i,@e);">>,
						   [U, H, N]),
				  mysql:query(M, <<"SELECT @i,@e;">>)
			  end),
    P !
      {queried_user, #{userid => UserID, expiry => Expired}},
    mysql:stop(M),
    exit(normal).

get_expiry({P, U}) ->
    {ok, M} = init_mysql(),
    % Filthy transaction system!
    {atomic, {ok, [<<"@q">>], [[Expired]]}} =
	mysql:transaction(M,
			  fun () ->
				  ok = mysql:query(M,
						   <<"CALL `furtheworld`.`check_user_expiry`(rpad(?"
						     ",16,0x0), @q)">>,
						   [U]),
				  mysql:query(M, <<"SELECT @q">>)
			  end),
    P ! {nonce_checked, Expired},
    mysql:stop(M),
    exit(normal).

set_expiry({P, U, N}) ->
    {ok, M} = init_mysql(),
    {ok, [<<"@s">>], [[Successful]]} = mysql:query(M,
						   <<"call `furtheworld`.`set_nonce`(rpad(?,16,0x0)"
						     ",?,@s); SELECT @s;">>,
						   [U, N]),
    if Successful =:= 1 -> P ! nonce_set;
       true -> P ! nonce_not_set
    end,
    mysql:stop(M),
    exit(normal).

