%%%-------------------------------------------------------------------
%% @doc furtheworld API v0.3 rate limiter
%% @end
%%%-------------------------------------------------------------------
-module(furtheworld_apiv0_3rl).

-behaviour(gen_statem).

-include("furtheworld_apiv0_3rl.hrl").

-export([start/1]).

-define(TMAX, 2).

-export([callback_mode/0, init/1]).

-export([max/0, rate_limit/3]).

-define(SERVER, ?MODULE).

start(Args) ->
    gen_statem:start_link({local, ?SERVER}, ?MODULE, Args,
			  []).

max() -> ?TMAX.

init(Peer) ->
    {ok, rate_limit,
     #furtheworld_apiv0_3rl_t{peer = Peer, tries = 0},
     [{state_timeout, 2000, []}]}.

rate_limit({call, From}, bump,
	   #furtheworld_apiv0_3rl_t{tries = T})
    when T > (?TMAX) ->
    {keep_state_and_data,
     [{reply, From, {ok, rate_limit}},
      {state_timeout, 60000, []}]};
rate_limit(state_timeout, [], Data) ->
    {keep_state, Data#furtheworld_apiv0_3rl_t{tries = 0},
     []};
rate_limit({call, From}, bump,
	   #furtheworld_apiv0_3rl_t{tries = T} = Data) ->
    TP = T + 1,
    {keep_state, Data#furtheworld_apiv0_3rl_t{tries = TP},
     [{reply, From, {ok, {throttle, T}}},
      {state_timeout, 1000, []}]}.

callback_mode() -> state_functions.
