%%%-------------------------------------------------------------------
%% @doc furtheworld API v0.3 route endpoint - unsupported calls, etc.
%% @end
%%%-------------------------------------------------------------------
-module(furtheworld_apiv0_3reetc).

-export([parse_error/1]).

parse_error(<<"unimplemented">>) ->
    <<"This feature of the site is unimplemented.">>;
parse_error(<<"throttled">>) ->
    <<"The site has been throttled.">>;
parse_error(_) -> <<"An unspecified error occured.">>.
