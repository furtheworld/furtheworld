%%%-------------------------------------------------------------------
%% @doc furtheworld public API
%% @end
%%%-------------------------------------------------------------------

-module(furtheworld_app).

-behaviour(application).

-export([start/2, stop/1]).

start(_StartType, _StartArgs) ->
  Dispatch = cowboy_router:compile([
    {'_', [{"/v0_3/:field/[:subfield]", furtheworld_apiv0_3web, #{}}]}
  ]),
  {ok, _Process} = cowboy:start_clear(
    furtheworld_listener,
    [{port, 8080}],
    #{env => #{dispatch => Dispatch}}),
  furtheworld_sup:start_link().

stop(_State) ->
  ok.

%% internal functions
